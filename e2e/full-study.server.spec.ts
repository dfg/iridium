import type { Page } from '@playwright/test';
import { test, expect } from '@playwright/test';
import { prisma } from '~/db.server';
import yaml from 'js-yaml'



const TEST_CONFIG = () => {
    const raw = `
---
distribution:
  # The keys (ex. 18-24) correspond with the select options in the /register form
  18-24: 57
  25-34: 83
  35-44: 67
  45-54: 43
  55-64: 35
  65-67: 25
# gender percentages in percent. < 100% leaves room for the 'diverse' option
male-percentage: 40
female-percentage: 40
# annotation seetings
minimum-annotations: 1
maximum-annotations: 5
# qualtifail delta in minutes (between first and last submission)
qualityfail-delta: 10
# Image distribution between private and public
image-dist: 0.5
# Maximum submissions.
max-submissions: 60
# respondi redirect links. the respondi ticket gets appended in the webapp
redirects:
  # C1
  screenout: mingle.respondi.com/s/1589997/ospe.php3?c_0002=0&return_tic=
  # C2
  quotafull: mingle.respondi.com/s/1589997/ospe.php3?c_0002=2&return_tic=
  # C2.1 failed attention checks (disabled)
  quality: mingle.respondi.com/s/1589997/ospe.php3?c_0002=3&return_tic=
  # C3 
  done: mingle.respondi.com/s/1589997/ospe.php3?c_0002=1&return_tic=
    `

    const config = yaml.load(raw) as any
    return config.distribution
}

const evalImage = async (page: Page) => {
    // Check input[name="questionOne"] >> nth=1
    await page.locator('input[name="questionOne"]').nth(1).check();

    // Check input[name="confidenceOne"] >> nth=0
    await page.locator('input[name="confidenceOne"]').first().check();

    // Click text=Weiter
    await page.locator('text=Weiter').click();
    // await expect(page).toHaveURL(/.+qtwo\?.+/)
    //   await expect(page).toHaveURL(`${imgserver}/images/09d781a6-4a4d-48db-81ac-ed485da7bc62/qtwo?confidence=1&question=stimme%20absolut%20nicht%20zu`);

    // Click label:has-text("Kollegen")
    await page.locator('label:has-text("Kollegen")').click();

    // Check input[name="confidenceTwo"] >> nth=1
    await page.locator('input[name="confidenceTwo"]').nth(1).check();

    // Click button:has-text("Weiter")
    await page.locator('button:has-text("Weiter")').click();

    // await expect(page).toHaveURL(/.+qone.*/)
}

test.describe('Create Multiple users', async () => {
    // test.setTimeout(1000 * 60 * 60 * 24 * 7);

    test.beforeAll(async () => {
        await prisma.surveyOne.deleteMany()
        await prisma.surveyTwo.deleteMany()
        await prisma.surveyThree.deleteMany()
        await prisma.submission.deleteMany()
        await prisma.user.deleteMany()
    })

    test.afterAll(async () => {
        await prisma.surveyOne.deleteMany()
        await prisma.surveyTwo.deleteMany()
        await prisma.surveyThree.deleteMany()
        await prisma.submission.deleteMany()
        await prisma.user.deleteMany()
    })

    const GROUPS = TEST_CONFIG()

    for (let ageGroup = 0; ageGroup < 6; ageGroup++) {
        const currentGroup = [...Object.keys(GROUPS)][ageGroup]
        const testSize = GROUPS[currentGroup];


        test.describe(`a full user (${currentGroup} - ${ageGroup}/${testSize} )`, async () => {
            // test.setTimeout(1000 * 60 * 60 * 24 * 7);

            for (let index = 0; index < testSize; index++) {
                test(`${currentGroup} - ${index}/${testSize}`, async ({ browser }) => {

                    const context = await browser.newContext()
                    const page = await context.newPage()   //console.log(`${currentGroup} - ${index}/${testSize}`)

                    // Go to ${imgserver}/?tic=foxtrott
                    await page.goto(`/?tic=foxtrott`);

                    // Click text=Starten
                    await page.locator('text=Starten').click();
                    await expect(page).toHaveURL(`/register?tic=foxtrott`);

                    // Select 25-34
                    await page.locator('select[name="age"]').selectOption(currentGroup);

                    if (Math.floor(testSize - (testSize * 0.4)) >= index) {
                        // Check #gender-m
                        await page.locator('#gender-m').check();
                    } else {
                        await page.locator('#gender-w').check();
                    }

                    // Check input[name="country"]
                    await page.locator('input[name="country"]').check();

                    // Check input[name="socialNetworks"]
                    await page.locator('input[name="socialNetworks"]').check();

                    // Check input[name="tos"]
                    await page.locator('input[name="tos"]').check();

                    // Click text=Weiter
                    await page.locator('text=Weiter').click();
                    await page.waitForTimeout(1000)
                    await expect(page).toHaveURL(`/survey/1`);

                    await page.waitForTimeout(1000)

                    // Go to ${imgserver}/survey/3
                    await page.goto(`/survey/3`, { timeout: 15000 });

                    // Check input[name="sharing_personal_information_bothered"] >> nth=0
                    await page.locator('input[name="sharing_personal_information_bothered"]').first().check();

                    // Check input[name="sharing_personal_information_freely"] >> nth=1
                    await page.locator('input[name="sharing_personal_information_freely"]').nth(1).check();

                    // Check input[name="openess"] >> nth=2
                    await page.locator('input[name="openess"]').nth(2).check();

                    // Check input[name="worried_about_privacy"] >> nth=1
                    await page.locator('input[name="worried_about_privacy"]').nth(1).check();

                    // Check input[name="compare_privacy_with_others"] >> nth=2
                    await page.locator('input[name="compare_privacy_with_others"]').nth(2).check();

                    // Click text=18.) Für mich ist es das Wichtigste, Dinge vor anderen privat zu halten.1 - stim >> label >> nth=0
                    await page.locator('text=18.) Für mich ist es das Wichtigste, Dinge vor anderen privat zu halten.1 - stim >> label').first().click();

                    // Click div:nth-child(8) > .z-0 > li:nth-child(5) > .w-full
                    await page.locator('div:nth-child(8) > .z-0 > li:nth-child(5) > .w-full').click();

                    // Check input[name="public_self_information"] >> nth=2
                    await page.locator('input[name="public_self_information"]').nth(2).check();

                    // Check input[name="information_access_concerns"] >> nth=1
                    await page.locator('input[name="information_access_concerns"]').nth(1).check();

                    // Check input[name="out_of_context_information"] >> nth=2
                    await page.locator('input[name="out_of_context_information"]').nth(2).check();

                    // Check input[name="overthinking_information"] >> nth=3
                    await page.locator('input[name="overthinking_information"]').nth(3).check();

                    // Check input[name="paranoid"] >> nth=4
                    await page.locator('input[name="paranoid"]').nth(4).check();

                    // Click div:nth-child(14) > .z-0 > li:nth-child(5) > .w-full
                    await page.locator('div:nth-child(14) > .z-0 > li:nth-child(5) > .w-full').click();

                    // Click div:nth-child(15) > .z-0 > li >> nth=0
                    await page.locator('div:nth-child(15) > .z-0 > li').first().click();

                    // Check input[name="self_confident_info"] >> nth=0
                    await page.locator('input[name="self_confident_info"]').first().check();

                    // Check input[name="self_confident_thoughts"] >> nth=2
                    await page.locator('input[name="self_confident_thoughts"]').nth(2).check();

                    // Check input[name="sharing_feelings_with_others"] >> nth=3
                    await page.locator('input[name="sharing_feelings_with_others"]').nth(3).check();

                    // Click button:has-text("Weiter")
                    await page.locator('button:has-text("Weiter")').click();


                    /**
                     * 
                     *  Image evaluation
                     * 
                     * 
                     */
                    for (let i = 0; i < 60; i++) {
                        await evalImage(page)
                    }
                    console.log('done')

                    // await expect(page).toHaveURL(`/done`)
                })
            }
        });
    }
});