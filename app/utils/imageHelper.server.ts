import type { Submission, User } from "@prisma/client";
import _, { filter } from "lodash";
import invariant from "tiny-invariant";
import { prisma as db } from "~/db.server";
import {
  MIN_ANNOTATIONS,
  MAX_ANNOTATIONS,
  IMAGE_DISTRIBUTION,
  MAX_SUBMISSIONS
} from "~/utils/constants.server"
import { Mutex } from 'async-mutex'

const mutex = new Mutex()

export type Image = {
  attributes: ImageAttribute;
  submission: Submission[];
  id: string;
};

export type ImageAttribute = {
  filename: string;
  isPrivate: boolean;
};

export class ImageHelper {
  studySize: number;
  weight: number;
  poolSize = 6000


  /**
   *
   * @param studySize the number of images to be studied for a user
   * @param weight the percentage of private images to be studied for a user
   */
  constructor(studySize: number, weight: number) {
    this.studySize = studySize;
    this.weight = weight;
  }

  /**
   * Returns a random image from the dataset with the bias towards the images with the most annotations.
   * @param user the user id
   * @returns a preferred, random image
   */
  async getImage(user: User): Promise<Image> {
    if (!user) throw new Error("getImage needs a User");

    const { currentPrivateSubmissions, currentPublicSubmissions } = user;

    const nPrivate = (this.studySize * this.weight) - currentPrivateSubmissions;
    const nPublic = (this.studySize * this.weight) - currentPublicSubmissions;
    const userDatasets = (await db.submission.findMany({
      where: {
        userId: user.id,
      },
      select: {
        datasetId: true,
      }
    })).map(({ datasetId }) => datasetId)

    // find all images with at least one annotation and at most MAX_ANNOTATIONS from teh dataset
    let privateImagesDB = (await db.dataset.findMany({
      where: {
        attributes: {
          path: ['isPrivate'],
          equals: true
        },

      },
      include: {
        _count: {
          select: {
            submission: true
          }
        }
      },
    })).sort((a, b) => b._count.submission - a._count.submission);

    privateImagesDB = privateImagesDB
      .filter(img => img._count.submission < MAX_ANNOTATIONS)
      .filter(img => !userDatasets.includes(img.id))
      .slice(0, nPrivate);

    let publicImagesDB = (await db.dataset.findMany({
      where: {
        attributes: {
          path: ['isPrivate'],
          equals: false
        }
      },
      include: {
        _count: {
          select: {
            submission: true
          }
        }
      },
    })).sort((a, b) => b._count.submission - a._count.submission);

    publicImagesDB = publicImagesDB
      .filter(img => img._count.submission < MAX_ANNOTATIONS)
      .filter(img => !userDatasets.includes(img.id))
      .slice(0, nPublic);

    let imagesDB = _.shuffle([...privateImagesDB, ...publicImagesDB]);


    if (imagesDB.length === 0) {
      const count = 1
      const pictureCount = await db.dataset.count()
      const skip = Math.max(0, Math.floor(Math.random() * pictureCount) - count)
      const results = (await db.dataset.findMany({
        skip,
        take: count,
      }))
      const candidate = results[0]

      return {
        attributes: candidate.attributes as ImageAttribute,
        submission: [],
        id: candidate.id,
      }
    }


    const candidate = _.maxBy(imagesDB, img => img._count.submission) || imagesDB[0];

    invariant(candidate, "No image found");
    invariant(candidate.attributes, "No image attributes found");

    const candidateSubmissions = await db.submission.findMany({
      where: {
        datasetId: candidate.id,
      }
    })

    return {
      attributes: candidate.attributes as ImageAttribute,
      id: candidate.id,
      submission: candidateSubmissions
    };
  }

  /**
   * Utility function to get the number of submissions for a user
   * @param datasetId the id of the dataset
   * @returns the number of submissions for the given dataset
   */
  async getSubmissionCountFromDataset(datasetId: string) {
    const dataset = await db.dataset.findFirstOrThrow({
      include: { _count: { select: { submission: true } } },
      where: {
        AND: [
          { id: datasetId },
        ]
      }
    })

    return dataset._count.submission;
  }

  /**
   * Creates a submission for the given image and user. Only use this for inserting!
   * 
   * @param datasetId the id of the dataset
   * @param submissionData submission data
   * @param user user data
   * @returns false if the submission was not created / rejected, because MAX_ANNOTATIONS was reached
   */
  async createImage(datasetId: string,
    submissionData: {
      confidenceOne: number;
      confidenceTwo: number;
      questionOne: string;
      questionTwo: any;
      userId: string;
      datasetId: string;
    }, user: User) {
    invariant(user, "User is required");
    invariant(datasetId, "Dataset is required");
    invariant(submissionData, "Submission data is required");

    // avoid concurrent submissions which could lead to $Dataset(id)->|Submission[]| > MAX_ANNOTATIONS$
    return mutex.runExclusive(async () => {
      const count = await this.getSubmissionCountFromDataset(datasetId);

      if (count >= MAX_ANNOTATIONS) return false;

      return db.submission.create({
        data: {
          ...submissionData,
          userId: user.id,
          datasetId: datasetId,
        }
      });
    })
  }
}

export const imageHelper = new ImageHelper(MAX_SUBMISSIONS, IMAGE_DISTRIBUTION);
