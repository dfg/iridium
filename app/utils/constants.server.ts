import { z } from "zod"

// Minimum number of annotations for an image
const MIN_ANNOTATIONS = 1
// Maximum number of annotations for an image
const MAX_ANNOTATIONS = 3
// If delta in minutes is less, than redirect to qualityfail link
// testing in /done
const QUALITYFAIL_DELTA = 10 // TODO: Implement
// percentage between public and private images
const IMAGE_DISTRIBUTION = 0.5
// maximum submissions of a user before a redirect is done
const MAX_SUBMISSIONS = 60

const envSchema = z.object({
    IMAGE_SERVER: z.string().min(1, 'IMAGE_SERVER must be set'),
    DATABASE_URL: z.string().min(1, 'DATABASE_URL must be set'),
    SESSION_SECRET: z.string().min(15, 'SESSION_SECRET must be set'),
    CONFIG_URL: z.string().min(1, 'CONFIG_URL must be set'),
    NODE_ENV: z.enum(["development", "production", "test"]),
})

export const env = envSchema.parse(process.env)

export {
    MIN_ANNOTATIONS,
    MAX_ANNOTATIONS,
    IMAGE_DISTRIBUTION,
    MAX_SUBMISSIONS
}
