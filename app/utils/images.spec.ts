import type { User } from '@prisma/client'
import { test, describe, expect } from 'vitest'
import { prisma } from '~/db.server'
import { imageHelper } from './imageHelper.server'
import { MAX_ANNOTATIONS, MAX_SUBMISSIONS, MIN_ANNOTATIONS } from '~/utils/constants.server'
import { updateUserSubmissionCounts } from '~/images.server'
import invariant from 'tiny-invariant'


const TEST_SUBMISSION_DATA = async ({ id: userId }: User, datasetId: string) => ({
    confidenceOne: 1,
    confidenceTwo: 1,
    questionOne: 'test',
    questionTwo: { value: 'test' },
    userId,
    datasetId
})

const createImageEval = async (user: User, datasetId: string) => {
    const result = await prisma.submission.create({
        data: await TEST_SUBMISSION_DATA(user, datasetId)
    })

    await updateUserSubmissionCounts(user.id)
    return result
}

const createTestUser = () => prisma.user.create({
    data: {
        age:
            '18-24',
        country: true,
        gender: 'm',
        ticket: 'fox',
    }
})

const TINY_TEST = true

describe('test the image algorithm', async () => {
    let user: Promise<User> = createTestUser()
    let userSize = TINY_TEST ? 10 : 200
    let testSubmissionSize = TINY_TEST ? 20 : MAX_SUBMISSIONS

    // make submissions / seeding
    beforeAll(async () => {
        await prisma.submission.deleteMany()
        await prisma.surveyThree.deleteMany()
        await prisma.user.deleteMany()
    })


    // delete user(s)
    afterEach(async () => {
    })

    afterAll(async () => {
        // delete db entries
        await prisma.submission.deleteMany()
        await prisma.surveyThree.deleteMany()
        await prisma.user.deleteMany()
    })

    test('should return an image', async () => {
        const img = await imageHelper.getImage(await user)
        await createImageEval(await user, img.id)
        expect(img).toBeDefined()
    })

    describe('inserting the image', async () => {
        test('should insert an image', async () => {
            const u = await user
            const img = await imageHelper.getImage(u)
            const result = await imageHelper.createImage(img.id, await TEST_SUBMISSION_DATA(u, img.id), u)
            expect(result).toBeTruthy()
            invariant(result, "Submission should not be null")
            const submission = await prisma.submission.findFirst({ where: { id: result.id }, select: { id: true } })
            expect(submission).not.toBeNull()
        })

        test('should reject an image if the dataset has reached the maximum number of annotations', async () => {
            const u = await user
            const img = await imageHelper.getImage(u)
            for (let index = 0; index < MAX_ANNOTATIONS + 1; index++) {
                const result = await imageHelper.createImage(img.id, await TEST_SUBMISSION_DATA(u, img.id), u)
                invariant(result, "Submission should not be null")
                const submission = await prisma.submission.findFirst({ where: { id: result.id }, select: { id: true } })
                expect(submission).not.toBeNull()

                if (index === MAX_ANNOTATIONS) {
                    const faulty = await imageHelper.createImage(img.id, await TEST_SUBMISSION_DATA(u, img.id), u)
                    const dataSets = await prisma.dataset.findMany({
                        include: { _count: { select: { submission: true } } },
                        where: {
                            AND: [
                                { id: img.id },
                            ]
                        }
                    })
                    const count = dataSets[0]._count.submission
                    expect(count).toBeLessThanOrEqual(MAX_ANNOTATIONS)
                    expect(faulty).toBeFalsy()
                }
            }

        })
    })

    describe(`create submissions`, async () => {
        for (let i = 0; i < userSize; i++) {
            test.concurrent(`${i} - user flow`, async () => {
                const u = await createTestUser()
                for (let subCount = 0; subCount < testSubmissionSize;) {
                    const img = await imageHelper.getImage(u)
                    const res = await imageHelper.createImage(img.id, await TEST_SUBMISSION_DATA(u, img.id), u)
                    if (res) subCount++
                }
            }, 1000 * 60 * 3)
        }
    })

    test('shoud have datasets with no more than MAX_ANNOTATIONS submissions', async () => {
        const images = await prisma.dataset.findMany({
            include: {
                _count: {
                    select: {
                        submission: true
                    }
                }
            }
        })
        images.forEach(img => {
            expect(img._count.submission).toBeLessThanOrEqual(MAX_ANNOTATIONS)
        }
        )
    })

    test('should have at leat some submissions with 5 entries', async () => {
        const entries = await prisma.dataset.findMany({
            include: {
                _count: { select: { submission: true } }
            },
        })
        let biggestN: number = 0
        let countFive = 0
        for (let index = 0; index < entries.length; index++) {
            const element = entries[index];
            if (element._count.submission > biggestN) biggestN = element._count.submission
            if (element._count.submission >= 5) countFive++
        }
        expect(biggestN).toBeGreaterThanOrEqual(MIN_ANNOTATIONS)
        expect(biggestN).toBeLessThanOrEqual(MAX_ANNOTATIONS)
        //expect at least two images with 5 entries
        // expect(countFive).toBeGreaterThanOrEqual(25)
    })


    test.fails('shoud fail if the user is falsy', async () => {
        const tmpUser: User = {
            age: '',
            country: false,
            createdAt: new Date(),
            currentPrivateSubmissions: 0,
            currentPublicSubmissions: 0,
            gender: 'w',
            id: '',
            ticket: '',
            updatedAt: new Date()
        }
        expect(imageHelper.getImage(tmpUser)).toThrow()
    })
})

export { }
