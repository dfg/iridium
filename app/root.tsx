import type { LinksFunction, MetaFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
} from "@remix-run/react";
import { RecoilRoot } from "recoil";
import Layout from "./components/Layout";

import tailwindStylesheetUrl from "./styles/tailwind.css";

export const links: LinksFunction = () => {
  return [{ rel: "stylesheet", href: tailwindStylesheetUrl }];
};

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Image Classification Study",
  viewport: "width=device-width,initial-scale=1",
});

export function CatchBoundary() {
  const caught = useCatch();
  return (
    <html>
      <head>
        <title>Oops!</title>
        <Meta />
        <Links />
      </head>
      <body>
        <div className="pose mx-auto">
          <h1>
            Cannot find the page requested! {caught.status} {caught.statusText}
          </h1>
        </div>
        <Scripts />
      </body>
    </html>
  );
}

export function ErrorBoundary({ error }: any) {
  console.error(error);
  return (
    <html>
      <head>
        <title>Oops!</title>
        <Meta />
        <Links />
      </head>
      <body>
        <div className="prose mx-auto mt-5 text-center">
          Leider ist eine Fehler aufgetreten. Sind sie eingeloggt? Bei Fragen
          melden Sie sich bitte bei uns.
          <div>
            <a href="mailto:kqiku@cs.uni-goettingen.de">
              kqiku(at)cs.uni-goettingen.de
            </a>
          </div>
          <a
            className="btn btn-primary mt-8"
            href="https://c109-055.cloud.gwdg.de/"
          >
            Zurueck zur Homepage
          </a>
        </div>
        <Scripts />
      </body>
    </html>
  );
}

export default function App() {
  return (
    <html lang="en" className="h-full">
      <head>
        <Meta />
        <Links />
      </head>
      <body className="h-full">
        <RecoilRoot>
          <Layout>
            <Outlet />
            <ScrollRestoration />
            <Scripts />
            <LiveReload />
          </Layout>
        </RecoilRoot>
      </body>
    </html>
  );
}
