describe("Register a user from home and go to survey", () => {
  it('should do a whole study', async () => {
    cy.visit('/register?tic=foxtrott')
    cy.get('select[name="age"]').select('25-34')
    cy.get('#gender-m').check()
    cy.get('input[name="country"]').check()
    cy.get('input[name="socialNetworks"]').check()
    cy.get('input[name="tos"]').check()
    cy.contains('Weiter').click()
    cy.url().should('include', '/survey/1')
    cy.visit('/survey/3')

    cy.get('input[type="radio"]').click({ multiple: true })
    cy.get('body > div.container.mx-auto.p-5.pb-14 > div > form > button').click()

    // images

    for (let i = 0; i < 60; i++) {
      cy.get('body > div.container.mx-auto.p-5.pb-14 > div > div > div:nth-child(2) > form > fieldset > div > ul > li:nth-child(1)').click()
      cy.get('body > div.container.mx-auto.p-5.pb-14 > div > div > div:nth-child(2) > form > div > ul > li:nth-child(1) > label').click()
      cy.get('body > div.container.mx-auto.p-5.pb-14 > div > div > div:nth-child(2) > form > button').click()

      cy.get('[type="checkbox"]').first().check()
      cy.get('[type="radio"]').first().check()

      cy.get('body > div.container.mx-auto.p-5.pb-14 > div > div > div:nth-child(2) > form > button').click()
    }
  })
});
