import { defineConfig } from 'cypress'

export default defineConfig({
  e2e: {
    baseUrl: 'https://c109-055.cloud.gwdg.de',
    specPattern: 'cypress/e2e/**/*.cy.{js,jsx,ts,tsx}',
  },
})
